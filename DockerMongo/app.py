"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
#import config
from pymongo import MongoClient
import os
import logging

###
# Globals
###
app = flask.Flask(__name__)
#CONFIG = config.configuration()
app.secret_key = 'secret'
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
collection = db.control
collection.delete_many({})

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_dist = request.args.get('brevet_dist', 0, type=float)
    begin_date = request.args.get('begin_date', 0, type=str)
    begin_time = request.args.get('begin_time', 0, type=str)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    print(begin_date+" "+begin_time)
    open_time = acp_times.open_time(km,  brevet_dist, arrow.get(begin_date+" "+begin_time).isoformat)
    close_time = acp_times.close_time(km, brevet_dist,arrow.get(begin_date+" "+begin_time).isoformat)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

@app.route('/display')
def display():
    """
    Display opening and closing times from
    database.
    """
    app.logger.debug("Displaying times.")
    flask.g.kms = []
    flask.g.open = []
    flask.g.close = []
    for entry in collection.find():
       
        flask.g.kms.append(entry['km'])
        flask.g.open.append(entry['open_time'])
        flask.g.close.append(entry['close_time'])
    
    if flask.g.open == []:
        return flask.render_template("empty.html")

    return flask.render_template("display.html")

#######


@app.route('/new', methods=['POST'])
def new():
    open_times = request.form.getlist("open")
    close_times = request.form.getlist("close")
    kms = request.form.getlist("km")
    app.logger.debug("submitting times.")
    app.logger.debug(kms)
    
    if kms[0] == '':
        app.logger.debug("empty")
        return flask.render_template("nothing.html")

    for item in range(20):
        record = {
        'open_time' : open_times[item],
        'close_time' : close_times[item],
        'km' : kms[item]
        }
        collection.insert_one(record)  

    return flask.redirect(flask.url_for("index"))

#############################################

if __name__ == "__main__":
    app.run(host="0.0.0.0",debug=True)
