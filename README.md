Brevet time calculator with Ajax

Created by Zac Sims, zsims@uoregon.edu

This is a system to calculate open and close times for control points along standard ACP-sactioned brevets of length 200km, 300km, 400km, 600km and 1000km. The calculation for these times is as outlined by Randonneurs USA (RUSA). A description of this calculation can be found here: https://rusa.org/pages/acp-brevet-control-times-calculator. (Note: A varation of the USA algorithm is being used here, not the French version.

Considerations:
As the longest ACP-sanctioned brevet is 100km, a maximum distance for any control point is 1000km.
If a control point is specificed at the brevet start, the open and close times for that control point will be the start time of the brevet, and one hour after the start time, per RUSA guidelines.
